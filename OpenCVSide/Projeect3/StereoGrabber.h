//StereoGrabber.h is the header file which contains the definition 
//for variables and functions used to obtain images for processing.
#pragma once
#include "stdafx.h"
#include <iostream>

#include "opencv2/core/core_c.h"
#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc_c.h"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/video/tracking.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/flann/flann.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/objdetect/objdetect.hpp"
//#include "opencv2/legacy/compat.hpp"
//#include "opencv2/core/internal.hpp"
#include "opencv2/core/core_c.h"
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui_c.h"
#include "opencv2/highgui/highgui.hpp"


#include "opencv2/core/core_c.h"
#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc_c.h"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/video/tracking.hpp"
#include "opencv2/video/background_segm.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/objdetect/objdetect.hpp"
//#include "opencv2/legacy/legacy.hpp"
//#include "opencv2/legacy/compat.hpp"
//#include "opencv2/legacy/blobtrack.hpp"
///#include "opencv2/contrib/contrib.hpp"

#define WIDTH 352
#define HEIGHT 288
struct StereoGrabber{
	
	void stereoGrabberInitFrames();
	void stereoGrabberGrabFrames();	//grab a frame from the stream
	void stereoGrabberStopCam();
	IplImage* imageLeft;
	IplImage* imageRight;
};
