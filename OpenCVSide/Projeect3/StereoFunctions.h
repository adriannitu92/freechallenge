//StereoFunctions.h contains the definitions for variables 
//and functions used to rectify, correlate, and re-project stereo images.
#pragma once
#include "stdafx.h"

#include <iostream>
#include <sstream>
#include <string>
#include <fstream> 
#include <vector>

#include "BlobResult.h"

#include "opencv2/core/core_c.h"
#include "opencv2/core/core.hpp"
//#include "opencv2/imgproc/imgproc_c.h"
//#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/video/tracking.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/flann/flann.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/calib3d/calib3d_c.h"
#include "opencv2/objdetect/objdetect.hpp"
//#include "opencv2/legacy/compat.hpp"
//#include "opencv2/core/internal.hpp"
#include "opencv2/core/core_c.h"
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui_c.h"
#include "opencv2/highgui/highgui.hpp"


#include "opencv2/core/core_c.h"
#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc_c.h"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/video/tracking.hpp"
#include "opencv2/video/background_segm.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/objdetect/objdetect.hpp"
//#include "opencv2/legacy/legacy.hpp"
//#include "opencv2/legacy/compat.hpp"
//#include "opencv2/legacy/blobtrack.hpp"
//#include "opencv2/contrib/contrib.hpp"

#include "StereoGrabber.h"
#include "Interpolation.h"
#include "opencv2/core/types_c.h"
//#include <tchar.h>
using namespace std;

struct StereoFunctions{

	CvMat* _M1;
	CvMat* _M2;
	CvMat* _T;
	CvMat* mx1; 
	CvMat* mx2;
	CvMat* my1; 
	CvMat* my2; 
	CvMat* _Q;			//reprojection matrix
	CvMat* _CamData;

	CvSize imageSize;	//size of input images 
	IplImage	*img1,	//left image
				*img2,	//right image
				*rgb,
				*thres_img,
				*blobs_img,
				*img_detect,
				*real_disparity;
	CvMat	*img1r,		//rectified left image
			*img2r,		//rectified right image
			*disp,		//disparity map
			*vdisp,		//scaled disparity map for viewing
			*pair,
			*depthM;


	void stereoInit(StereoGrabber*);
	void stereoCorrelation(StereoGrabber*);	
	void CorrelationBMState();
	void stereoSavePointCloud();
	void stereoCalibration(StereoGrabber*);
	void stereoDetect();
	double ComputeDis(CvMat*,int row, int col);
	double reprojectionVars[6];
	

	int stereoPreFilterSize, 
		stereoPreFilterCap,  
		stereoDispWindowSize,
		stereoNumDisparities,
		stereoDispTextureThreshold,
		stereoDispUniquenessRatio,
		stereoSavePointCloudValue, 
		stereoSaveOriginal;


	int fileNO, threshold, blobArea;

	
	
};
