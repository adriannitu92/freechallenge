//StereoGrabber.cpp contains the algorithms for obtaining images for 
//processing from various cameras, including networked cameras and fire wire cameras
#include "stdafx.h"
#include "StereoGrabber.h"

CvCapture *captureleft= NULL, *captureright= NULL; //capture1 <-> left, capture2 <-> right

void StereoGrabber::stereoGrabberInitFrames()
		{
		captureleft= cvCaptureFromCAM(2);
		assert(captureleft!=NULL); 
		cvWaitKey(100);
		captureright= cvCaptureFromCAM(1);
		assert(captureright!=NULL);
		
	//assure capture size is correct...
		cvSetCaptureProperty(captureleft,CV_CAP_PROP_FRAME_WIDTH,WIDTH);
		cvSetCaptureProperty(captureleft,CV_CAP_PROP_FRAME_HEIGHT,HEIGHT);
		cvSetCaptureProperty(captureright,CV_CAP_PROP_FRAME_WIDTH,WIDTH);
		cvSetCaptureProperty(captureright,CV_CAP_PROP_FRAME_HEIGHT,HEIGHT);

		}

void StereoGrabber::stereoGrabberGrabFrames()
		{
		imageLeft = cvQueryFrame(captureleft);
		imageRight = cvQueryFrame(captureright);
		}
		
void StereoGrabber::stereoGrabberStopCam()	
		{
		cvReleaseCapture( &captureleft ); 
		cvReleaseCapture( &captureright );
		}